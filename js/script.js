$(function(){
  
  var $btnMenu = $('.jsMenu');
  
  $btnMenu.click(function () {
      $(this).toggleClass('menu-is-active');
      $('._nav').toggleClass('menu-is-open');
  });
  
  
  if ($(window).width() > 992) {

        $('.jsDropdown')
            .mouseover(function () {
                $(this).find('.jsDropdownMenu').fadeIn(300);
            })
            .mouseleave(function () {
                $('.jsDropdownMenu').fadeOut(300);
            })
        ;

        $('.jsShowSubMenu')
            .mouseover(function () {
                $(this).children('.jsSub').show();
            })
            .mouseleave(function () {
                $(this).children('.jsSub').hide();
            })
        ;

    } else {

        $('.jsMobileDropdownMenu').click(function () {
            $(this).toggleClass('-open');
            $(this).parent().find('.jsDropdownMenu').toggle();

        });
        $('.jsMobileSub').click(function () {
            $(this).toggleClass('-open');
            $(this).next().toggle();
        });

    }
  
});